//
//  DetailViewController.swift
//  FatUnicornTest
//
//  Created by kim on 29-02-16.
//  Copyright © 2016 SkyeApps. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController
{
    @IBOutlet var imageView: UIImageView?
    
    var image : UIImage?
    
    @IBOutlet var taskLabel : UILabel?
    
    var taskName : String?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if ((image) != nil)
        {
            self.imageView!.image = image
            
            self.taskLabel?.text = taskName
        }
        else
        {
            print("Image Not Found")
        }


    }
    
    @IBAction func goBackToTableView(sender : UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)?.viewWillAppear(true)
    }

   
}
