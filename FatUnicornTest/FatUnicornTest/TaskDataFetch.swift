//
//  TaskDataFetch.swift
//  FatUnicornTest
//
//  Created by kim on 23-02-16.
//  Copyright © 2016 SkyeApps. All rights reserved.
//

import UIKit
import Foundation
import CoreData

var dataURL: String = "https://bitbucket.org/fatunicorn/iostest/raw/78f902ddb31ac96bb23b901054442dee52664271/data/tasklist.json"

var newDataObject: NSManagedObject?

class TaskDataFetch: NSObject
{
    let  appDelegate = (UIApplication.sharedApplication().delegate) as! AppDelegate
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext

    
    func getDataFromServer()
    {
        print("Data From Server Called")
        
        
        let url:NSURL = NSURL(string: dataURL)!
        let request = NSMutableURLRequest(URL: url)
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            
            
                self.parseJsonAndSaveInCoreData(data!)
            
            
            dispatch_async(dispatch_get_main_queue())
                {
                // go to something on the main thread
                print("Reload called from Notification")
                NSNotificationCenter.defaultCenter().postNotificationName("RealoadTable", object: nil)

            }
            
        });
        
        // start Loading
        task.resume()
    }
    
    func parseJsonAndSaveInCoreData(fetchedData: NSData)
    {
        var finalArrayToSave : [AnyObject]!
        
        do {
            let jsonArray = try NSJSONSerialization.JSONObjectWithData(fetchedData, options: .AllowFragments) as? [[String: AnyObject]]
            
            //only filter if Data found in CoreData
            let predicate = NSPredicate(format: "TRUEPREDICATE")

            if(self.getDataFromCoreData(predicate).count > 0)
            {
                print("Data Saved in CoreData")
                finalArrayToSave = self.filterArraywithToPreventDuplicationValue(jsonArray!)
            }
            else
            {
                finalArrayToSave = jsonArray
            }
            
            print(finalArrayToSave)
            
            //saving in core data        
            
            let entityDiscription = NSEntityDescription.entityForName ("TaskObject", inManagedObjectContext: self.managedObjectContext)
            
            
            for tempDic in finalArrayToSave
                
            {
                
                let stringURL : String = (tempDic["image"] as? String)!
                
                let imageURL = NSURL(string: stringURL)
                
                let imgData = NSData(contentsOfURL:imageURL!)
                
                let image : UIImage = UIImage(data: imgData!)!
                
                let imageData =  UIImageJPEGRepresentation(image, 1.0)
                
                
                if((newDataObject == nil))
                {
                    let newData = NSManagedObject(entity: entityDiscription!, insertIntoManagedObjectContext: self.managedObjectContext)
                    
                    newData.setValue(tempDic["done"], forKey: "done")
                    newData.setValue (imageData, forKey: "image")
                    newData.setValue(tempDic["task"], forKey: "task")
                    
                    print("********************\(newData)")
                }
                else
                {
                    newDataObject!.setValue(tempDic["done"], forKey: "done")
                    newDataObject!.setValue (imageData, forKey: "image")
                    newDataObject!.setValue(tempDic["task"], forKey: "task")
                    
                    print("********************\(newDataObject)")
                    
                    
                }
                
                
            }
            
            // try self.managedObjectContext.save()
            
            self.appDelegate.saveContext()
            
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "AppLaunchedFirsttime")

            
        } catch {
            print("error serializing JSON: \(error)")
        }
        
    }
    
    //To Prevent Duplicate entry in Coredata
    // compares saved CoreData array to newly fetch array from server
    // And only returns new objecrt Array to save in core data
    func filterArraywithToPreventDuplicationValue(newDataArray:[AnyObject]) -> [AnyObject]
    {
        var fetchedArray = [AnyObject]()
        var arrayToSave = [AnyObject]()
        
        let predicate = NSPredicate(format: "TRUEPREDICATE")
        fetchedArray = self.getDataFromCoreData(predicate)
        
        
        for newObject in newDataArray
        {
            for oldObject in fetchedArray
            {
                if (newObject !== oldObject)
                {
                    print("oldArray :\(oldObject) adn New:\(newObject)")
                    arrayToSave.append(newObject)
                    
                }
            }
        }
        print("NewArray:\(arrayToSave)")
        return arrayToSave
    }
    
    func getDataFromCoreData(predicate: NSPredicate) -> [AnyObject]
    {
        // Create a new fetch request using the TaskObject entity
        let fetchRequest = NSFetchRequest(entityName: "TaskObject")
        var dataArray = [AnyObject]()
        
        fetchRequest.returnsObjectsAsFaults = false
        
        fetchRequest.predicate = predicate
        
        // Execute the fetch request, and cast the results to an array of TaskObject objects
        if let fetchResults = (try? managedObjectContext.executeFetchRequest(fetchRequest))
        {
            print("fetch count:",(fetchResults.count))
            
            //  print(fetchResults)
            
            dataArray = fetchResults
            
            
            
        }
        return dataArray
        
    }
    

}
