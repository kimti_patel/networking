//
//  TableViewController.swift
//  FatUnicornTest
//
//  Created by kim on 23-02-16.
//  Copyright © 2016 SkyeApps. All rights reserved.
//

import UIKit
import CoreData

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var loadingLabel: UILabel!
    @IBOutlet var segmentedControl : UISegmentedControl!
    
    
    
    
    // Creating Empty Dictionary array
    var tableDataArray : NSMutableArray = [ ]
    
    var holdDataArray = []
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    let taskDataFetch : TaskDataFetch = TaskDataFetch()

    
    override func awakeFromNib()
    {
        print("VC awake From Nib")
         NSNotificationCenter.defaultCenter().addObserver(self, selector:"reloadMyTable", name: "RealoadTable", object: nil)
        

        NSNotificationCenter.defaultCenter().addObserver(self, selector:"loadingDone", name: "loadingDone", object: nil)


    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        segmentedControl.selectedSegmentIndex = 2
        
        
        segmentedControl.addTarget(self, action: "segmentSelected:", forControlEvents:.TouchUpInside )
        
        segmentedControl.sendActionsForControlEvents(UIControlEvents.ValueChanged)
        
        let pred = NSPredicate(format: "TRUEPREDICATE")

        reloadMyTable(pred)

    }
   
    
    func reloadMyTable(predicate: NSPredicate)
    {
        print("table reload called")
        
        let tempArray = taskDataFetch.getDataFromCoreData(predicate)
        
        if (tempArray.count > 0)
        {
        
            tableDataArray = NSMutableArray(array: tempArray)
        
            self.tableView.reloadData()

            holdDataArray = tableDataArray
        
            print("TableArray: \(tableDataArray)")
        
            self.loadingLabel.hidden = true

        }
        
    }
    

    
    func refreshTableArray()-> NSMutableArray
    {
        if(tableDataArray.count > 0)
        {
            tableDataArray.removeAllObjects()
        }
        
        //after reoving all objects, creating fresh array to filer from
        
        tableDataArray = NSMutableArray(array: holdDataArray)
        return tableDataArray
    }
   
    @IBAction func segmentChanged(sender: AnyObject)
    {
        print("segment control called")
        
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            print("0")
            
            
            let predicate = NSPredicate(format:"done = 0")

            self.reloadMyTable(predicate)

            print("False : \(tableDataArray)")
            
            
        case 1:
            print("1")
            
            
            let predicate = NSPredicate(format:"done = 1")
            
            self.reloadMyTable(predicate)
            
        case 2:
            
            
            let pred = NSPredicate(format: "TRUEPREDICATE")
            
            self.reloadMyTable(pred)
            
        default:
            break;
        }
        

    }
  
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {

        super.touchesBegan(touches, withEvent: event)

        print("Current:\(segmentedControl.selectedSegmentIndex)")

        print("Self.selectedSegmentIndex:\(segmentedControl.selectedSegmentIndex)")

        if segmentedControl.selectedSegmentIndex != 2
        {
            
            segmentedControl.sendActionsForControlEvents(UIControlEvents.ValueChanged)
        }

    }
    
    // MARK: TableView DataSource and Delegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tableDataArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let reuseIdentifier = "cell"
        
        var cell:UITableViewCell? = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier) as UITableViewCell?
        
        if (cell != nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: reuseIdentifier)
        }
    
        let rowObject = self.tableDataArray[indexPath.row]
        
        cell!.textLabel?.text = rowObject.valueForKey("task") as? String
        
        let done : Bool = (rowObject.valueForKey("done") as? Bool)!
        
                if (done == true)
                {
                   // print("done")
                    cell!.detailTextLabel?.text = "Done"
                }
                else
                {
                  //  print("Not Done")
                    cell!.detailTextLabel?.text = "Not Done"
                    
                }
        
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let detailView : DetailViewController = self.storyboard!.instantiateViewControllerWithIdentifier("DetailViewController") as! DetailViewController
        
        let rowObject = self.tableDataArray[indexPath.row]

        
        detailView.image = UIImage (data: rowObject.valueForKey("image") as! NSData)
        
        detailView.taskName = rowObject.valueForKey("task") as? String

        
        self.navigationController?.pushViewController(detailView, animated: true)
        
        //self.presentViewController(detailView, animated: true, completion: nil)
        
    }
   
   

    
}

