//
//  FatUnicornTestTests.swift
//  FatUnicornTestTests
//
//  Created by kim on 23-02-16.
//  Copyright © 2016 SkyeApps. All rights reserved.
//

import XCTest
@testable import FatUnicornTest

class FatUnicornTestTests: XCTestCase
{
    var tableVC : TableViewController!
    
    override func setUp()
    {
        let storyBoard : UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        
        tableVC = (storyBoard.instantiateViewControllerWithIdentifier("TableViewController") as? TableViewController)!
        
        tableVC.loadView()
        
        
    }
    func testIfFirstNameLabelNOTConnectedWithStoryboard()
    {
        XCTAssertNil(tableVC.loadingLabel,"loadingLabel should be NOT connected to storyboard");
    }

    
    
    func testIfFirstNameLabelConnectedWithStoryboard()
    {
        XCTAssertNotNil(tableVC.loadingLabel,"loadingLabel should be connected to storyboard");
    }
//    func testActionIsSetForTestBtuuon()
//    {
//        XCTAssertEqual(tableVC.se.action, Selector("testButtonAlert"), "TestButton action is not set")
//    }
//    
//    func testSegmentActuionIsSet()
//    {
//        // given
//                
//        // when
//        let segmentedControl = UISegmentedControl()
//        segmentedControl.selectedSegmentIndex = 0
//        segmentedControl.addTarget(tableVC, action: "segmentSelected:", forControlEvents: .ValueChanged)
//        segmentedControl.sendActionsForControlEvents(.ValueChanged)
//        
//        // then
//        XCTAssertTrue(mockUserDefaults.sortWasChanged, "Sort value in user defaults should be altered")
//    }

    func testIfSegmentControlIsConnectedAndAction()
    {
        let actions = tableVC.segmentedControl.actionsForTarget(self, forControlEvent: .TouchUpInside)
        
       // XCTAssertNotNil(actions, "Action array is empty")
        
        if let actions = actions
        {
            XCTAssert(actions.count > 0)
            
           if (actions.contains("segmentSelected"))
           {
                XCTAssert(true)
            }
            else
           {
                XCTAssert(false)
            }
        }
    }
}
